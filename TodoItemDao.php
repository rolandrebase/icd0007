<?php
interface TodoItemDao {
    public function addItem($contactItem);
    public function getItems();
}
