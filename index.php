<?php
require_once ("SqliteTodoItemDao.php");
require_once ("TodoItemDao.php");
require_once ("ContactItem.php");

$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "showListPage";

if ($cmd == "showAddPage") {
    include("lisamiseVormiLeht.html");
}
else if ($cmd == "showListPage") {
    include ("nimekirjaLeht.html");
}
else if ($cmd == "save") {
    $daoItem = new SqliteTodoItemDao();
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];

    $phone1 = $_POST['phone1'];
    $phone2 = $_POST['phone2'];
    $phone3 = $_POST['phone3'];
    $contactItem = new ContactItem($firstName, $lastName);
    $daoItem->addItem($contactItem);
    $daoItem->addPhone($phone1);
    $daoItem->addPhone($phone2);
    $daoItem->addPhone($phone3);
    header("Location: ?cmd=showListPage");
}
