<?php

class ContactItem
{
    public $id;
    public $firstName;
    public $lastName;
    public $phone = [];

    public function __construct($firstName = null, $lastName = null, $id = null) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function addPhone($phone) {
        $this->phone[] =  $phone;
    }
}
