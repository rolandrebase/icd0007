<?php

require_once ("TodoItemDao.php");

class SqliteTodoItemDao implements TodoItemDao
{
    private $connection;
    private $lastInsertedContactId;

    public function __construct() {
        $this->connection = new PDO("sqlite:contacts");
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getItems() {
        $statement = $this->connection->prepare(
            "select id, firstName, lastName, phone from contacts
                      left join phones on phones.contacts_id = contacts.id");
        $statement->execute();
        $contactItems = [];
        foreach ($statement as $row) {
            $id = $row['id'];
            if (isset($contactItems[$id])) {
                $item = $contactItems[$id];
                $item->addPhone($row["phone"]);
            } else {
                $item = new ContactItem($row["firstName"], $row["lastName"], $row["id"]);
                $item->addPhone($row["phone"]);
                $contactItems[$item->id] = $item;
            }
        }
        return array_values($contactItems);
    }

    public function addItem($contactItem) {
        if (isset($contactItem)) {
            $statement = $this->connection->prepare(
                "insert into contacts (firstName, lastName) values (:firstName, :lastName)");
            $statement->bindValue(":firstName", $contactItem->firstName);
            $statement->bindValue(":lastName", $contactItem->lastName);
            $statement->execute();
            $this->lastInsertedContactId = $this->connection->lastInsertId();
        }
    }
    public function addPhone($phone) {
        $statement = $this->connection->prepare(
            "insert into phones (contacts_id, phone) values (:id, :phone)");
        $statement->bindValue(":id", $this->lastInsertedContactId);
        $statement->bindValue(":phone", $phone);
        $statement->execute();
    }
}
